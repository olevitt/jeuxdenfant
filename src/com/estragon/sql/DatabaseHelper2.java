package com.estragon.sql;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.estragon.jeuxdenfant.Appli;
import com.estragon.jeuxdenfant.MainActivity;
import com.estragon.jeuxdenfant.R;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import core.Animation;
import core.TypeAnimation;

public class DatabaseHelper2 extends OrmLiteSqliteOpenHelper {

	// name of the database file for your application -- change to something appropriate for your app
	private static final String DATABASE_NAME = "jeudenfants";
	// any time you make changes to your database objects, you may have to increase the database version
	private static final int DATABASE_VERSION = 12;

	// the DAO object we use to access the SimpleData table
	private RuntimeExceptionDao<Animation, Integer> simpleRuntimeDao = null;
	private Dao<TypeAnimation, Integer> typeAnimationDao = null;
	
	static DatabaseHelper2 helper = null;

	public DatabaseHelper2(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION,R.raw.ormlite_config);
	}

	public static synchronized DatabaseHelper2 getHelper() {
		if (helper == null) {
			helper = OpenHelperManager.getHelper(Appli.getInstance(), DatabaseHelper2.class);
		}
		return helper;
	}
	
	/**
	 * This is called when the database is first created. Usually you should call createTable statements here to create
	 * the tables that will store your data.
	 */
	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
		try {
			Log.i(MainActivity.NOMLOG, "onCreate");
			TableUtils.createTable(connectionSource, Animation.class);
			TableUtils.createTable(connectionSource, TypeAnimation.class);
		} catch (SQLException e) {
			Log.e(MainActivity.NOMLOG, "Can't create database", e);
			throw new RuntimeException(e);
		}

	}

	/**
	 * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
	 * the various data to match the new version number.
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
		try {
			Log.i(MainActivity.NOMLOG, "onUpgrade");
			TableUtils.dropTable(connectionSource, Animation.class, true);
			TableUtils.dropTable(connectionSource, TypeAnimation.class, true);
			// after we drop the old databases, we create the new ones
			onCreate(db, connectionSource);
		} catch (SQLException e) {
			Log.e(MainActivity.NOMLOG, "Can't drop databases", e);
			throw new RuntimeException(e);
		}
	}

	

	/**
	 * Returns the RuntimeExceptionDao (Database Access Object) version of a Dao for our SimpleData class. It will
	 * create it or just give the cached value. RuntimeExceptionDao only through RuntimeExceptions.
	 */
	public RuntimeExceptionDao<Animation, Integer> getAnimationDao() {
		if (simpleRuntimeDao == null) {
			try {
				simpleRuntimeDao = getRuntimeExceptionDao(Animation.class);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return simpleRuntimeDao;
	}
	
	/**
	 * Returns the RuntimeExceptionDao (Database Access Object) version of a Dao for our SimpleData class. It will
	 * create it or just give the cached value. RuntimeExceptionDao only through RuntimeExceptions.
	 */
	public Dao<TypeAnimation, Integer> getTypeAnimationDao() {
		if (typeAnimationDao == null) {
			try {
				typeAnimationDao = getDao(TypeAnimation.class);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return typeAnimationDao;
	}

	/**
	 * Close the database connections and clear any cached DAOs.
	 */
	@Override
	public void close() {
		super.close();
		simpleRuntimeDao = null;
	}
}

package com.estragon.jeuxdenfant;

import java.net.URI;
import java.net.URISyntaxException;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.estragon.sql.DatabaseHelper2;
import com.j256.ormlite.stmt.PreparedQuery;

import core.Animation;

public class JeuActivity extends Activity  {

	int idAnimation;
	Animation animation;
	
	TextView description;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		
		//Récupération des paramètres
		try {
			idAnimation = getIntent().getIntExtra("idAnimation", -1);
			if (idAnimation == -1)
				throw new IllegalArgumentException();
			PreparedQuery<Animation> query = DatabaseHelper2.getHelper().getAnimationDao().queryBuilder().where().eq("id", idAnimation).prepare();	
			animation = DatabaseHelper2.getHelper().getAnimationDao().query(query).get(0);
		}
		catch (Exception e) {
			Toast.makeText(this, "Activité introuvable", Toast.LENGTH_LONG).show();
			finish();
			return;
		}
		
		
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		//setActionBarContentView(R.layout.activite);
		setTitle(animation.getNom());
		//getActionBar().setOnActionBarListener(this);
		
		//share = getActionBar().addItem(ActionBarItem.Type.Share).setDrawable(com.cyrilmottier.android.greendroid.R.drawable.gd_action_bar_share);
		
		
		description = (TextView) findViewById(R.id.description);
		description.setText(animation.getRegles());
		
	}

	public void intentShare() {
		try {
			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			URI uri = new URI("http","www.64cases.com","/animation/API/partage.php","id="+animation.getInternalId(),null);
			intent.putExtra(Intent.EXTRA_TEXT, uri.toString());
			intent.putExtra(Intent.EXTRA_TITLE, animation.getNom());
			intent.putExtra(Intent.EXTRA_SUBJECT, animation.getNom());
			Intent intentChoisi = Intent.createChooser(intent, "Partage");
			if (intentChoisi != null) startActivity(intentChoisi);
		}
		catch (URISyntaxException e) {
			e.printStackTrace();
		}
		catch (ActivityNotFoundException e) {
			
		}
	}


	/*@Override
	public void onActionBarItemClicked(int position) {
		// TODO Auto-generated method stub
		if (position == -1) {
			//Bouton home
			finish();
		}
		else if (position == 0) {
			intentShare();
		}
	}*/

	
	
}

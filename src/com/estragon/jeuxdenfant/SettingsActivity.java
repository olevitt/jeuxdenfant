package com.estragon.jeuxdenfant;

import android.content.ComponentName;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;

public class SettingsActivity extends PreferenceActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setTitle("Preferences");
		addPreferencesFromResource(R.layout.preferences);
		Preference infos = (Preference) this.findPreference("infos");
		infos.setSummary("Version "+getVersionName());
	}

	
	public static String getVersionName() 
	{
		try {
			ComponentName comp = new ComponentName(Appli.getInstance().getApplicationContext(), SettingsActivity.class);
			PackageInfo pinfo = Appli.getInstance().getApplicationContext().getPackageManager().getPackageInfo(comp.getPackageName(), 0);
			return pinfo.versionName;
		} catch (android.content.pm.PackageManager.NameNotFoundException e) {
			return "?";
		}
	}
	
	public static int getVersionCode() 
	{
		try {
			ComponentName comp = new ComponentName(Appli.getInstance().getApplicationContext(), SettingsActivity.class);
			PackageInfo pinfo = Appli.getInstance().getApplicationContext().getPackageManager().getPackageInfo(comp.getPackageName(), 0);
			return pinfo.versionCode;
		} catch (android.content.pm.PackageManager.NameNotFoundException e) {
			return 0;
		}
	}
	
}

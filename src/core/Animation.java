package core;

import org.json.JSONException;
import org.json.JSONObject;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Animation")
public class Animation {

	@DatabaseField(generatedId=true, allowGeneratedIdInsert=true, columnName="id")
	int internalId;
	
	@DatabaseField
	String nom;
	
	@DatabaseField(columnName="type")
	int type;
	
	@DatabaseField
	String regles;
	
	
	public Animation() {
		
	}
	
	public Animation(JSONObject json) throws JSONException {
		this.internalId = json.getInt("id");
		this.nom = json.getString("nom");
		this.type = json.getInt("type");
		this.regles = json.getString("regles");
	}
	
	public Animation(String nom,int type,String regles) {
		this.nom = nom;
		this.type = type;
	}
	
	public String getNom() {
		return nom;
	}
	
	public int getType() {
		return type;
	}
	
	public int getInternalId() {
		return internalId;
	}
	
	public String getRegles() {
		return regles;
	}
}

package core;

import org.json.JSONException;
import org.json.JSONObject;

import com.j256.ormlite.field.DatabaseField;

public class TypeAnimation {

	@DatabaseField(id=true)
	int id;
	
	@DatabaseField()
	String nom;
	
	public TypeAnimation() {
		
	}
	
	public TypeAnimation(JSONObject json) throws JSONException {
		this.id = json.getInt("id");
		this.nom = json.getString("nom");
	}
	
	public String getNom() {
		return nom;
	}
	
	public int getId() {
		return id;
	}
}
